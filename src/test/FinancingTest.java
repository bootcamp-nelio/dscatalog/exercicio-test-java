package test;

import main.entities.Financing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FinancingTest {

  private Financing obj;

  private Double totalAmount;
  private Double income;
  private Integer months;

  private Integer monthsF;
  private Double totalAmountF;
  private Double incomeF;

  @BeforeEach
  void setup() {
	this.totalAmount = 100_000D;
	this.income = 2_000D;
	this.months = 80;

	this.monthsF = 20;
	this.totalAmountF = 50_000D;
	this.incomeF = 3_000D;

	this.obj = new Financing(totalAmount, income, months);
  }

  @Test
  void shouldCreateObjectWithCorrectDataWhenDataIsValid() {
	assertEquals(totalAmount, obj.getTotalAmount());
	assertEquals(income, obj.getIncome());
	assertEquals(months, obj.getMonths());
  }

  @Test
  void shouldThrowIllegalArgumentExceptionWhenDataIsNotValid() {
	assertThrows(IllegalArgumentException.class, () -> new Financing(totalAmount, income, monthsF));
  }

  @Test
  void shouldUpdateValueWhenTotalAmountIsValid() {
	this.obj.setTotalAmount(this.totalAmount - this.totalAmountF);
	assertEquals(this.totalAmountF, obj.getTotalAmount());
  }

  @Test
  void shouldThrowIllegalArgumentExceptionWhenTotalAmountIsNotValid() {
	assertThrows(IllegalArgumentException.class, () -> this.obj.setTotalAmount(this.totalAmount + this.totalAmountF));
  }

  @Test
  void shouldUpdateIncomeWhenDataIsValid() {
	this.obj.setIncome(this.incomeF);
	assertEquals(this.incomeF, this.obj.getIncome());
  }

  @Test
  void shouldThrowExceptionWhenUpdatingIncomeIfDataIsNotValid() {
	assertThrows(IllegalArgumentException.class, () -> this.obj.setIncome(1000.0));
  }

  @Test
  void shouldUpdateMonthsWhenDataIsValid() {
	this.obj.setMonths(100);
	assertEquals(100, this.obj.getMonths());
  }

  @Test
  void shouldThrowExceptionWhenTheMonthsArentValid(){
	assertThrows(IllegalArgumentException.class, () -> this.obj.setMonths(10));
  }

  @Test
  void shouldCalculateCorrectlyTheInputValue(){
	assertEquals(20_000D, obj.entry());
  }

  @Test
  void shouldCalculateCorrectlyOValueofInstallment(){
	assertEquals(1_000, obj.quota());
  }
}
